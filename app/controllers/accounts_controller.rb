class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /accounts
  # GET /accounts.json
  def index
     params[:q] ||= {}
    if params[:q][:created_at_lteq].present?
      params[:q][:created_at_lteq] = params[:q][:created_at_lteq].to_date.end_of_day
    end
    if params[:q][:created_at_gteq].present?
      params[:q][:created_at_gteq] = params[:q][:created_at_gteq].to_date.beginning_of_day
    end   
      @q = Account.ransack(params[:q])
      @accounts  = @q.result
      @total_balance = 0
      @total_paper = 0
      @total_vtb = 0
      @total_plastic = 0
      @total_accounts = 0 
      @accounts.find_each do |a|
         @total_accounts += 1 
         @total_balance += a.balance
         
          case a.card_type
          when   "Пластиковая КЛ"
            @total_plastic += 1  
          when "Бумажная КЛ"             
            @total_paper += 1
          when "ВТБ"
            @total_vtb += 1
          end
      end  
      
      @accounts  = @accounts.page(params[:page]) 
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
   # @account.card_number = [*('a'..'z'),*('0'..'9')].shuffle[0,8].join
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account      = Account.new(account_params)
    @account.user = current_user
    @transaction  = @account.transactions.build( :user_id => current_user.id, :amount => @account.balance,
                                                :transaction_type => Account::OPERATIONS.first,
                                                :account_id =>  @account.id      )
    respond_to do |format|
      if @account.save
       #   @transaction.save
        #  @transaction = @account.transactions.new
     #  @transaction.user_id = current_user.id
     #  @transaction.amount = @account.balance
     #  @transaction.transaction_type = Account::OPERATIONS.last
     #  @transaction.save

        format.html { redirect_to @account, notice: 'Новый аккаунт успешно создан' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
#   def put_money_on
#     respond_to do |format|
#      #binding.pry
#       if @account.update(account_params)
#         format.html { redirect_to @account, notice: 'Account was successfully updated.' }
#         format.json { render :show, status: :ok, location: @account }
#       else
#         format.html { render :edit }
#         format.json { render json: @account.errors, status: :unprocessable_entity }
#       end
#     end
#   end
#   # POST /accounts.json
#   def put_money_on
#     @account = Account.new(account_params)

#     respond_to do |format|
#       if @account.save
#         format.html { redirect_to @account, notice: 'Account was successfully created.' }
#         format.json { render :show, status: :created, location: @account }
#       else
#         format.html { render :new }
#         format.json { render json: @account.errors, status: :unprocessable_entity }
#       end
#     end
#   end
# def


  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Аккаунт был успешно обновлен' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end


 
  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Аккаунт был успешно удален' }
      format.json { head :no_content }
    end
  end

 
 
  private
    
    def load_activities
      @activities = PublicActivity::Activity.order('created_at DESC').limit(20)
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit! #(:card_number, :last_name, :first_name, :middle, :dob, :balance)
    end
end
