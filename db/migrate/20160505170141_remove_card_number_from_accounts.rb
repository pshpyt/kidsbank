class RemoveCardNumberFromAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :card_number, :string
  end
end
