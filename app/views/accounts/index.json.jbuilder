json.array!(@accounts) do |account|
  json.extract! account, :id, :card_number, :last_name, :first_name, :middle, :dob, :balance
  json.url account_url(account, format: :json)
end
