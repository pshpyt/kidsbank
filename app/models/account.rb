class Account < ActiveRecord::Base
  # relations
  CARD_TYPES = [ "Пластиковая КЛ", "Бумажная КЛ", "ВТБ" ]
  OPERATIONS = [ "Пополнение", "Снятие" ]  
  
  has_many :transactions
  belongs_to :user
  paginates_per 50 
  

  validates :balance, :first_name, :last_name, :card_type,   presence: true 
  validates :balance, numericality: { only_integer: true }
  validates :card_number, uniqueness: true
  # include PublicActivity::Model
 # tracked owner: Proc.new { |controller, model| controller.current_user ? controller.current_user : nil }
  
   #has_many :cards , dependent: :destroy


  def full_name
    "#{first_name} #{last_name}"
  end

end
