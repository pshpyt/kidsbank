class Transaction < ActiveRecord::Base
  belongs_to :account,  required: true  #,  dependent: :destroy
  paginates_per 30
  belongs_to :user
  validates :amount, presence: true 
  
# ransacker :line_count do
#   query = "(SELECT COUNT(lines.id) FROM lines WHERE lines.document_id = documents.id GROUP BY lines.document_id)"
#   Arel.sql(query)
# end
# 
# ransacker :created_at do
#   Arel.sql('date(created_at)')
# end
# 

# ransacker :average do
#   Arel.sql('average')
# end
  
  ransack_alias :account, :account_card_type
end
