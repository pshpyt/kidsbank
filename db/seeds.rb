# Generated with RailsBricks Initial seed file to use with Devise User Model

# Temporary admin account
#destroy old users
User.destroy_all

# create admin user
u = User.new(
    email: "admin@gmail.com",
    password: "admin4you",
    admin: true
)
u.skip_confirmation!
u.save!

# Test user accounts
(1..5).each do |i|
  u = User.new(
      email: "operator#{i}@gmail.com",
      password: "admin4you"
  )
  u.skip_confirmation!
  u.save!

  puts "Default users created" 
end
